<?php



/**
 * Class Url
 */
class Url extends MainObject
{
    const AD_TYPE_KUPIM = 'kupim',
        AD_TYPE_PREDAM = 'predam';

    private $protocol = 'http://';
    private $separator = '/';
    private $server = 'bazos.sk';
    private $category = null;
    private $subcategory = null;
    private $offset = null;
    public $params = null;
    private $type;



    public function __construct(SimpleXMLElement $element = null)
    {
        $this->params = new UrlParams();

        if (isset($element->server)) {
            $this->setServer($element->server);

            if($element->server == 'bazos.sk') {
                $this->params->setOrder(1);
            } else {
                $this->params->setOrder(null);
            }
        }

        if (isset($element->category)) {
            $this->setCategory($element->category);
        }

        if (isset($element->subcategory)) {
            $this->setSubcategory($element->subcategory);
        }

        if (isset($element->type)) {
            $this->setType($element->type);
        }

        if (isset($element->offset)) {
            $this->setOffset($element->offset);
        }

        if (isset($element->search)) {
            $this->params->setSearch($element->search);
        }

        if (isset($element->priceFrom)) {
            $this->params->setPriceFrom($element->priceFrom);
        }

        if (isset($element->priceTo)) {
            $this->params->setPriceTo($element->priceTo);
        }

        if (isset($element->location)) {
            $this->params->setLocation($element->location);
        }

        if (isset($element->locationSurrKm)) {
            $this->params->setLocationSurrKm($element->locationSurrKm);
        }


    }



    /**
     * Method setProtocol.
     *
     * @param $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }



    /**
     * Method setSeparator.
     *
     * @param $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }



    /**
     * Method setServer.
     *
     * @param $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }



    /**
     * Method getServer.
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }



    /**
     * Method setCategory.
     *
     * @param $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }



    /**
     * Method setSubcategory.
     *
     * @param $subcategory
     */
    public function setSubcategory($subcategory)
    {
        $this->subcategory = $subcategory;
    }



    /**
     * Method setType.
     *
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }



    /**
     * Method setOffset.
     *
     * @param $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }



    /**
     * Method getFullURL.
     * @return string
     */
    public function getFullURL()
    {
        $url = $this->protocol;

        if ($this->category != null) {
            $url .= $this->category . '.';
        }

        $url .= $this->server;

        if ($this->type != null) {
            $url .= '/' . $this->type;
        }

        if ($this->subcategory != null) {
            $url .= '/' . $this->subcategory;
        }

        if ($this->offset != null) {
            $url .= '/' . $this->offset;
        }

        $params = $this->params->getParams();

        if (!empty($params)) {

            $url .= '/?';

            reset($params);
            $first = key($params);

            foreach ($params as $key => $value) {
                if ($key !== $first) {
                    $url .= '&';
                }

                $url .= "$key=$value";
            }
        }

        return $url;
    }



    public function getBaseURL()
    {
        $url = $this->protocol;

        if ($this->category != null) {
            $url .= $this->category . '.';
        }

        $url .= $this->server;

        return $url;
    }


}