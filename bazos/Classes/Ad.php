<?php



/**
 * @package    Bazos.AdClass
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */
class Ad
{
    public $nazov;
    public $link;
    public $cena;
    public $lokalita;
    public $datum;
    public $server;



    public function setNazov($nazov)
    {
        $this->nazov = $nazov;
    }



    public function setLink($link)
    {
        $this->link = $link;
    }



    public function setCena($cena)
    {
        $this->cena = $cena;
    }



    public function setLokalita($lokalita)
    {
        $this->lokalita = $lokalita;
    }



    public function setDatum($datum)
    {
        $this->datum = $datum;
    }



    public function setServer($server)
    {
        $this->server = $server;
    }
}