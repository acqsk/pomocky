<?php



/**
 * Class UrlParams
 */
class UrlParams extends MainObject
{
    private $search = null;
    private $location = null;
    private $locationSurrKm = 25;
    private $priceFrom = null;
    private $priceTo = null;
    private $order = 1;



    /**
     * addParam method.
     *
     * @param $key
     * @param $value
     */
    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }



    /**
     * setSearch method.
     *
     * @param $search
     */
    public function setSearch($search)
    {
        $this->search = str_replace(" ", "+", $search);
    }



    /**
     * setLocation method.
     *
     * @param $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }



    /**
     * setLocationSurrKm method.
     *
     * @param $locationSurrKm
     */
    public function setLocationSurrKm($locationSurrKm)
    {
        $this->locationSurrKm = $locationSurrKm;
    }



    /**
     * setPriceFrom method.
     *
     * @param $priceFrom
     */
    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;
    }



    /**
     * setPriceTo method.
     *
     * @param $priceTo
     */
    public function setPriceTo($priceTo)
    {
        $this->priceTo = $priceTo;
    }



    /**
     * setOrder method.
     *
     * @param $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }



    /**
     * getParams method.
     * @return array
     */
    public function getParams()
    {
        $params = array();

        if ($this->search != null) {
            $params['hledat'] = $this->search;
        }

        if ($this->location != null) {
            $params['hlokalita'] = $this->location;
        }

        if ($this->locationSurrKm != null) {
            $params['humkreis'] = $this->locationSurrKm;
        }

        if ($this->priceFrom != null) {
            $params['cenaod'] = $this->priceFrom;
        }

        if ($this->priceTo != null) {
            $params['cenado'] = $this->priceTo;
        }

        if ($this->order != null) {
            $params['order'] = $this->order;
        }

        return $params;
    }
}