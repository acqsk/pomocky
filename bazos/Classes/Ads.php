<?php



/**
 * @package    Bazos.AdsClass
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */
class Ads implements Countable
{
    private $url;
    private $pages;
    private $items = array();
    private $pagesLimit = 100;
    private $itemsLimit = 1500;
    private $title = null;



    /**
     * Ads constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->pages = (new Pages($url))->getPages();
        $this->url = $url;
    }



    /**
     * setTitle method.
     *
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }



    /**
     * setItemsLimit method.
     *
     * @param $limit
     */
    public function setItemsLimit($limit)
    {
        $this->itemsLimit = $limit;
    }



    /**
     * setPagesLimit method.
     *
     * @param $limit
     */
    public function setPagesLimit($limit)
    {
        $this->pagesLimit = $limit;
    }



    /**
     * getItems method.
     *
     * @return array
     */
    public function getItems()
    {
        $counterPages = 0;
        $counterItems = 0;

        foreach ($this->pages as $page) {
            $counterPages++;

            try {
                $this->url->setOffset($page);
                $html = file_get_html($this->url->getFullURL());
            } catch (RuntimeException $ex) {
                echo $ex->getMessage();
                die();
            }

            $n = ($html->find('span.vypis table[class="inzeraty"]'));

            foreach ($n as $item) {

                $counterItems++;

                $title = $item->find("tbody tr td span.nadpis a")[0]
                    ->nodes[0]
                    ->_[4];

                $link = $item->find("tbody tr td span.nadpis a")[0]
                    ->attr['href'];

                $price = $item->find("tbody tr td span.cena b")[0]
                    ->nodes[0]
                    ->_[4];

                $created = $item->find("tbody tr td span.nadpis")[0]
                    ->parent
                    ->nodes[2]
                    ->nodes[0]
                    ->_[4];

                $created = str_replace(" - Dopyt - [", "", $created);
                $created = str_replace(" - Ponuka - [", "", $created);
                $created = str_replace("]", "", $created);

                $i = new Ad();

                $i->setNazov($title);
                $i->setLink($link);
                $i->setCena($price);
                $i->setDatum($created);
                $i->setServer($this->url->getServer());

                array_push($this->items, $i);

                // Break items limit
                if ($counterItems >= $this->itemsLimit && $counterItems > 0) {
                    break 2;
                }
            }

            // Break pages limit
            if ($counterPages >= $this->pagesLimit) {
                break;
            }
        }

        return $this->items;

    }



    /**
     * Render method.
     *
     * @return string
     */
    public function render()
    {
        $items = $this->getItems();

        $result = '';

        if ($this->title) {
            $result .= '<h1>' . $this->title . ' (' . $this->url->getServer() . ')</h1>';
        }

        $result .= '<table>';
        foreach ($items as $item) {
            $result .= '<tr>';
            $result .= '<td>' . $item->server . '</td>';
            $result .= '<td>' . $item->datum . '</td>';
            $result .= '<td><a target="_blank" href="' . $this->url->getBaseURL() . $item->link . '">' . $item->nazov . '</a></td>';
            $result .= '<td>' . $item->cena . '</td>';
            $result .= '</tr>';
        }
        $result .= '</table>';

        $result .= '<br /><br />';
        $result .= $this->url->getFullURL();
        $result .= '<br />';
        $result .= 'Pocet inzeratov: ' . $this->count();

        return $result;
    }



    /**
     * count method.
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }
}