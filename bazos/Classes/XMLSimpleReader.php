<?php

/**
 * @package    Joomla.Templates.IS
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */
class XMLSimpleReader
{

    public $configurationsFolder = "configurations";



    /**
     * setConfigurationFolder method.
     *
     * @param $folder
     */
    public function setConfigurationFolder($folder)
    {
        $this->configurationsFolder = $folder;
    }



    /**
     * getXMLFiles method.
     *
     * @return array
     */
    public function getXMLFiles()
    {
        if (!file_exists($this->configurationsFolder)) {
            throw new RuntimeException("Configurations folder not exists");
        }

        // Init array
        $xmlFiles = array();


        // Search for XML files
        foreach (new DirectoryIterator($this->configurationsFolder) as $fileInfo) {
            if (strtolower($fileInfo->getExtension()) == 'xml') {
                $xmlFiles[] = $fileInfo->getFilename();
            }
        }

        // Return array of XML files
        return $xmlFiles;
    }



    /**
     * readXMLData method.
     *
     * @param $filePath
     *
     * @return SimpleXMLElement
     */
    public function readXMLData($filePath)
    {
        // Check, if filePath is defined
        if (empty($filePath)) {
            throw new RuntimeException("File path is not defined");
        }

        // Check, if file exists
        if (!file_exists($filePath)) {
            throw new RuntimeException("File not exists.");
        }

        // Try to read the XML file
        try {
            return simplexml_load_file($filePath);
        } catch (Exception $ex) {
            throw new RuntimeException($ex->getMessage());
        }
    }
}